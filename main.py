import pandas as pd
import json
from collections import Counter


def read_files():
    be_tool = pd.read_csv("data/be_tool.csv")
    be_tool.dropna(how="all", inplace=True)
    be_tool = be_tool[be_tool["fix_time"].str.contains("2020-")]

    # old_crawl = pd.read_csv("data/old_crawl.csv")

    with open('data/2020/data_3.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())
    new_crawl = pd.DataFrame(data)

    return be_tool, new_crawl


def read_files_2():
    with open('data/2020/old/2020.json', encoding='utf-8') as data_file_old:
        data_old = json.loads(data_file_old.read())
    old_crawl = pd.DataFrame(data_old)

    with open('data/2020/data_3.json', encoding='utf-8') as data_file:
        data = json.loads(data_file.read())
    new_crawl = pd.DataFrame(data)

    return old_crawl, new_crawl


def compare(be_tool, new_crawl):
    for i in range(1, 10):
        month = "2020-0" + str(i)

        be_tool_specific_month = be_tool[be_tool["fix_time"].str.contains(month)]
        new_crawl_specific_month = new_crawl[new_crawl["created_time"].str.contains(month)]

        be_tool_ids = set(be_tool_specific_month["label"])
        new_crawl_ids = set(new_crawl_specific_month["sender_id"])
        new_crawl_ids_2 = set(new_crawl_specific_month["sender_id"]).union(
            set(new_crawl_specific_month["to_sender_id"]))

        if "K's Closet" in be_tool_ids:
            be_tool_ids.remove("K's Closet")
        if "1059845080701224" in new_crawl_ids:
            new_crawl_ids.remove("1059845080701224")

        diff_1 = new_crawl_ids.difference(be_tool_ids)
        diff_2 = new_crawl_ids_2.difference(be_tool_ids)
        diff_3 = be_tool_ids.difference(new_crawl_ids)
        diff_4 = be_tool_ids.difference(new_crawl_ids_2)
        a = 0


def compare_sender_id(old_crawl, new_crawl):
    old_crawl_no_sender_list = []
    new_crawl_no_sender_list = []
    for i in range(1, 10):
        month = "2020-0" + str(i)

        old_crawl_specific_month = old_crawl[old_crawl["created_time"].str.contains(month)]
        new_crawl_specific_month = new_crawl[new_crawl["created_time"].str.contains(month)]

        old_crawl_specific_month_sender_ids = set(old_crawl_specific_month["sender_id"]).union(
            set(old_crawl_specific_month["to_sender_id"]))
        new_crawl_specific_month_sender_ids = set(new_crawl_specific_month["sender_id"]).union(
            set(new_crawl_specific_month["to_sender_id"]))

        old_crawl_no_sender_list.append(str(len(old_crawl_specific_month_sender_ids)))
        new_crawl_no_sender_list.append(str(len(new_crawl_specific_month_sender_ids)))

    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s" % str(",".join(old_crawl_no_sender_list)))
    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s" % str(",".join(new_crawl_no_sender_list)))


def compare_conversation_len(old_crawl, new_crawl):
    new_crawl_conv_len_list = []
    old_crawl_conv_len_list = []

    old_crawl_specific_month = old_crawl[old_crawl["created_time"].str.contains("2020-09")]
    new_crawl_specific_month = new_crawl[new_crawl["created_time"].str.contains("2020-09")]

    old_crawl_specific_month_fb_conv_id = list(old_crawl_specific_month["fb_conversation_id"])
    old_crawl_specific_month_fb_conv_id_counter_keys = list(Counter(list(old_crawl_specific_month_fb_conv_id)).keys())[:30]
    old_crawl_specific_month_fb_conv_id_counter_values = list(Counter(list(old_crawl_specific_month_fb_conv_id)).values())[:30]


    for key in old_crawl_specific_month_fb_conv_id_counter_keys:
        new_crawl_sub_df = new_crawl_specific_month[new_crawl_specific_month["fb_conversation_id"] == key]

        new_crawl_conv_len_list.append(str(len(new_crawl_sub_df)))

    old_crawl_conv_len_list = [str(x) for x in old_crawl_specific_month_fb_conv_id_counter_values]

    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s \n" % str(",".join(old_crawl_specific_month_fb_conv_id_counter_keys)))
    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s \n" % str(",".join(new_crawl_conv_len_list)))
    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s \n" % str(",".join(old_crawl_conv_len_list)))

def compare_sender_id_diff(old_crawl, new_crawl):
    old_crawl_no_sender_diff = []
    new_crawl_no_sender_diff = []
    for i in range(1, 10):
        month = "2020-0" + str(i)

        old_crawl_specific_month = old_crawl[old_crawl["created_time"].str.contains(month)]
        new_crawl_specific_month = new_crawl[new_crawl["created_time"].str.contains(month)]

        old_crawl_specific_month_sender_ids = set(old_crawl_specific_month["sender_id"]).union(
            set(old_crawl_specific_month["to_sender_id"]))
        new_crawl_specific_month_sender_ids = set(new_crawl_specific_month["sender_id"]).union(
            set(new_crawl_specific_month["to_sender_id"]))

        diff_1 = old_crawl_specific_month_sender_ids.difference(new_crawl_specific_month_sender_ids)
        diff_2 = new_crawl_specific_month_sender_ids.difference(old_crawl_specific_month_sender_ids)

        old_crawl_no_sender_diff.append(str(diff_1))
        new_crawl_no_sender_diff.append(str(diff_2))

    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s \n" % str(",".join(new_crawl_no_sender_diff)))
    with open("id_to_crawl_again", "a") as text_file:
        text_file.write("%s \n" % str(",".join(old_crawl_no_sender_diff)))

def main():
    # be_tool, new_crawl = read_files()
    # compare(be_tool, new_crawl)

    old_crawl, new_crawl = read_files_2()
    # compare_sender_id(old_crawl, new_crawl)
    # compare_conversation_len(old_crawl, new_crawl)
    compare_sender_id_diff(old_crawl, new_crawl)


if __name__ == '__main__':
    main()
